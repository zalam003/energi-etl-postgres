create table tokens
(
    address varchar(42),
    name text,
    symbol text,
    decimals bigint,
    function_sighashes text[]
);