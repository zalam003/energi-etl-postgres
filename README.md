# Energi ETL for PostgreSQL

The steps below will allow you to bootstrap a PostgreSQL database with full historical and real-time Energi data:
blocks, transactions, logs, token_transfers, and traces.

The whole process will take between 24 and 72 hours.

**Prerequisites**:

- Python 3.6+
- Postgres 11
- psql

### 1. Install Python 3

```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.8
sudo pip install psycopg2-binary
```

### 2. Install PostgreSQL

```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RELEASE=$(lsb_release -cs)
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list
sudo apt update
sudo apt -y install postgresql-11
```
### 3. Create PostgreSQL database and tables

- Create the database and the tables:

```bash
createdb nrg
```

```
psql
> CREATE SCHEMA energi AUTHORIZATION postgres;
```

```
cat schema/*.sql | psql -U postgres -d nrg -h 127.0.0.1 --port 5432 -a
```

A few performance optimization tips for initial loading of the data:

- Turn off fsync https://www.postgresql.org/docs/11/runtime-config-wal.html.
- Use UNLOGGED tables.
- Turn OFF auto backups and vacuum on Google Cloud SQL instance.

### 4. Apply indexes to the tables

- Run:

```bash
cat indexes/*.sql | psql -U postgres -d nrg -h 127.0.0.1 --port 5432 -a
```

Creating indexes is going to take between 12 and 24 hours. Depending on the queries you're going to run
you may need to create more indexes or [partition](https://www.postgresql.org/docs/11/ddl-partitioning.html) the tables.

### 5. Streaming

Use `ethereumetl stream` command to continually pull data from an Energi node and insert it to Postgres tables:

```bash
apt-get -y install python3-pip
pip3 install ethereum-etl[streaming]

ethereumetl stream \
    --provider-uri https://nodeapi.energi.network/rpc \
    --start-block 500000 \
    --lag 30 -e block,transaction,log,token_transfer,trace,contracts,tokens \
    --output=postgresql://postgres:postgres@127.0.0.1:5432/nrg?currentSchema=energi
```

- This command outputs blocks, transactions, logs, token_transfers to the console by default.
- Entity types can be specified with the `-e` option, 
e.g. `-e block,transaction,log,token_transfer,trace,contracts,tokens`.
- Use `--output` option to specify the Postgres database where to publish blockchain data, 
    - For Postgres: `--output=postgresql+pg8000://<user>:<password>@<host>:<port>/<database_name>`, 
    e.g. `--output=postgresql+pg8000://postgres:admin@127.0.0.1:5432/ethereum`.
    The [schema](https://github.com/blockchain-etl/ethereum-etl-postgres/tree/master/schema) 
    and [indexes](https://github.com/blockchain-etl/ethereum-etl-postgres/tree/master/indexes) can be found in this 
    repo [ethereum-etl-postgres](https://github.com/blockchain-etl/ethereum-etl-postgres). 
- The command saves its state to `last_synced_block.txt` file where the last synced block number is saved periodically.
- Specify either options:
  - `--start-block` or 
  - `--last-synced-block-file`

  `--last-synced-block-file` should point to the file where the block number, from which to start streaming the blockchain data, is saved.
- Use the `--lag` option to specify how many blocks to lag behind the head of the blockchain. It's the simplest way to 
handle chain reorganizations - they are less likely the further a block from the head.
- You can tune `--period-seconds`, `--batch-size`, `--block-batch-size`, `--max-workers` for performance.


- Refer to [blockchain-etl-streaming](https://github.com/blockchain-etl/blockchain-etl-streaming) for
instructions on deploying it to Kubernetes. 

Stream blockchain data to a Postgres database:

```bash
ethereumetl stream --start-block 500000 --output postgresql+pg8000://<user>:<password>@<host>:5432/<database>?currentSchema=<schema>
```

The [schema](https://github.com/blockchain-etl/ethereum-etl-postgres/tree/master/schema) 
and [indexes](https://github.com/blockchain-etl/ethereum-etl-postgres/tree/master/indexes) can be found in this 
repo [ethereum-etl-postgres](https://github.com/blockchain-etl/ethereum-etl-postgres).

Follow the instructions here to deploy it to Kubernetes: https://github.com/blockchain-etl/blockchain-etl-streaming.
