#!/bin/bash
#
# Description: Start/stop ETL script. Populates block, transaction, log, 
#              token_transfer, trace tables
#
# Change Log:
#              v1.0.0  ZAlam  Initial Script
#
#set -x

# Variables
STARTBLOCK=700000

# Main Script
case $1 in

    start)
        
        if [[ -f last_synced_block.txt ]]
        then
            STARTBLOCK=$(cat last_synced_block.txt)
            echo "Starting ETL from block: $STARTBLOCK"
            rm last_synced_block.txt
            nohup ethereumetl stream \
                --provider-uri https://nodeapi.energi.network/rpc \
                -e block,transaction,log,token_transfer,trace \
                --start-block $STARTBLOCK \
                --lag 30 \
                --output postgresql://postgres:postgres@127.0.0.1:5432/nrg >& /dev/null &
        else
            echo "Starting ETL from block: $STARTBLOCK"
            nohup ethereumetl stream \
                --provider-uri https://nodeapi.energi.network/rpc \
                -e block,transaction,log,token_transfer,trace \
                --start-block $STARTBLOCK \
                --lag 30 \
                --output postgresql://postgres:postgres@127.0.0.1:5432/nrg >& /dev/null &
        fi
        ;;

    stop)
        PID=$( ps -ef | grep ethereumetl | grep -v auto | grep -v "grep ethereumetl" | awk '{print $2}' )
        if [[ ! -z $PID ]]
        then
            echo "Stopping ETL process..."
            kill $PID
        fi
        ;;

esac
